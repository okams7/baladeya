import 'dart:convert';

Orders ordersFromJson(String str) {
    final jsonData = json.decode(str);
    return Orders.fromJson(jsonData);
}

List<Order> justOrdersFromJson(String str) {
  final jsonData = json.decode(str);
    Orders orders = Orders.fromJson(jsonData);
    return orders.data;
}

String ordersToJson(Orders data) {
    final dyn = data.toJson();
    return json.encode(dyn);
}

class Orders {
    List<Order> data;
    int res;

    Orders({
        this.data,
        this.res,
    });

    factory Orders.fromJson(Map<String, dynamic> json) => new Orders(
        data: new List<Order>.from(json["data"].map((x) => Order.fromJson(x))),
        res: json["res"],
    );

    Map<String, dynamic> toJson() => {
        "data": new List<dynamic>.from(data.map((x) => x.toJson())),
        "res": res,
    };
}

class Order {
    String fullName;
    String appDate;
    String streetNum;
    String locationNum;
    String landNum;
    String officeNum;
    String teleNum;
    String appType;
    String appState;
    String id;

    Order({
        this.fullName,
        this.appDate,
        this.streetNum,
        this.locationNum,
        this.landNum,
        this.officeNum,
        this.teleNum,
        this.appType,
        this.appState,
        this.id,
    });

    factory Order.fromJson(Map<String, dynamic> json) => new Order(
        fullName: json["full_name"],
        appDate: json["app_date"],
        streetNum: json["street_num"],
        locationNum: json["location_num"],
        landNum: json["land_num"],
        officeNum: json["office_num"],
        teleNum: json["tele_num"],
        appType: json["app_type"],
        appState: json["app_state"],
        id: json["id"],
    );

    Map<String, dynamic> toJson() => {
        "full_name": fullName,
        "app_date": appDate,
        "street_num": streetNum,
        "location_num": locationNum,
        "land_num": landNum,
        "office_num": officeNum,
        "tele_num": teleNum,
        "app_type": appType,
        "app_state": appState,
        "id": id,
    };
}
