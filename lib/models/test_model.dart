import 'dart:convert';

// List<Test> testFromJson(String str) {
//     final jsonData = json.decode(str);
//     return new List<Test>.from(jsonData.map((x) => Test.fromJson(x)));
// }

// String testToJson(List<Test> data) {
//     final dyn = new List<dynamic>.from(data.map((x) => x.toJson()));
//     return json.encode(dyn);
// }

List<Test> allTestsFromJson(String str) {
  final jsonData = json.decode(str);
  return new List<Test>.from(jsonData.map((x) => Test.fromJson(x)));
}

String allTestsToJson(List<Test> data) {
  final dyn = new List<dynamic>.from(data.map((x) => x.toJson()));
  return json.encode(dyn);
}

class Test {
    String reviewId;
    String providerId;
    String userId;
    String rate;
    String title;
    String description;
    String email;

    Test({
        this.reviewId,
        this.providerId,
        this.userId,
        this.rate,
        this.title,
        this.description,
        this.email,
    });

    factory Test.fromJson(Map<String, dynamic> json) => new Test(
        reviewId: json["review_id"],
        providerId: json["provider_id"],
        userId: json["user_id"],
        rate: json["rate"],
        title: json["title"],
        description: json["description"],
        email: json["email"],
    );

    Map<String, dynamic> toJson() => {
        "review_id": reviewId,
        "provider_id": providerId,
        "user_id": userId,
        "rate": rate,
        "title": title,
        "description": description,
        "email": email,
    };
}
