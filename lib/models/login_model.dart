// To parse this JSON data, do
//
//     final loginModel = loginModelFromJson(jsonString);

import 'dart:convert';

LoginModel loginModelFromJson(String str) {
  final jsonData = json.decode(str);
  return LoginModel.fromJson(jsonData);
}

String loginModelToJson(LoginModel data) {
  final dyn = data.toJson();
  return json.encode(dyn);
}

class LoginModel {
  int res;
  String id;
  String type;

  LoginModel({
    this.res,
    this.id,
    this.type,
  });

  factory LoginModel.fromJson(Map<String, dynamic> json) {
    if (json.containsKey('id') && json.containsKey('type')) {
      return LoginModel(
        res: json["res"],
        id: json["id"],
        type: json["type"],
      );
    } else {
      return LoginModel(
        res: json["res"],
        id: '0',
        type: '0',
      );
    }
  }

  Map<String, dynamic> toJson() => {
        "res": res,
        "id": id,
        "type": type,
      };
}
