import 'dart:convert';

Users usersFromJson(String str) {
    final jsonData = json.decode(str);
    return Users.fromJson(jsonData);
}

String usersToJson(Users data) {
    final dyn = data.toJson();
    return json.encode(dyn);
}

class Users {
    List<User> data;
    int res;

    Users({
        this.data,
        this.res,
    });

    factory Users.fromJson(Map<String, dynamic> json) => new Users(
        data: new List<User>.from(json["data"].map((x) => User.fromJson(x))),
        res: json["res"],
    );

    Map<String, dynamic> toJson() => {
        "data": new List<dynamic>.from(data.map((x) => x.toJson())),
        "res": res,
    };
}

class User {
    String id;
    String userName;
    String fullName;
    String idNum;
    String phone;

    User({
        this.id,
        this.userName,
        this.fullName,
        this.idNum,
        this.phone,
    });

    factory User.fromJson(Map<String, dynamic> json) => new User(
        id: json["id"],
        userName: json["user_name"],
        fullName: json["full_name"],
        idNum: json["id_num"],
        phone: json["phone"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "user_name": userName,
        "full_name": fullName,
        "id_num": idNum,
        "phone": phone,
    };
}
