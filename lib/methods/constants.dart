class MyConstants {
  ////////////Integers
  static const int genericValue = 0;
  static const int employee = 1;
  static const int engOffice = 2;
  static const int user = 3;
  static const int employeeLogin = 4;
  static const int employeeSignup = 5;
  static const int engOfficeLogin = 6;
  static const int engOfficeSignup = 7;
  static const int userLogin = 8;
  static const int userSignup = 9;
  static const int showOrders = 10;
  static const int newOrder = 11;
  static const int updateRequest = 12;
  static const int controlOrders = 13;
  static const int controlPrices = 15;
  static const int holidaies = 16;
  static const int username = 17;
  static const int password = 18;
  static const int full_name = 19;
  static const int id_num = 20;
  static const int phone = 21;
  static const int fullName = 22;
  static const int app_date = 23;
  static const int street_num = 24;
  static const int location_num = 25;
  static const int land_num = 26;
  static const int office_num = 27;
  static const int tele_num = 28;
  static const int app_type = 29;

////////////////Strings
  static const String s_username = "username";
  static const String s_fullName = "fullname";
  static const String s_office_num = "office_num";
}
