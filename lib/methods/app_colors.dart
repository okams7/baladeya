import 'package:flutter/material.dart';

const blueApp = Color(0xFF002157);
const blueAppDark = Color(0xFF001333);
const yellowApp = Color(0xFFFEF3B3);
const yellowAppDark = Color(0xFFF3D000);