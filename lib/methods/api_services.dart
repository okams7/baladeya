import 'package:baladeya/methods/constants.dart';
import 'package:baladeya/models/users.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'package:baladeya/models/login_model.dart';
import 'package:baladeya/models/orders_m.dart';
import 'dart:io';

// String url = 'https://jsonplaceholder.typicode.com/posts';
// String url = "http://n7tfl.com/okams/occasions/v1/services/getTest.php";
//for localhost
//192.168.1.232 // mac
//192.168.1.83 // windows (get your wifi ip)

//http://localhost/hebco-cbl/login.php?username=ali&password=123
//http://localhost/hebco-cbl/get_price.php
String mainAPI = "http://192.168.1.83/hebco-cbl/";

// Future<List<LoginModel>> getAllTest() async {
//   final response = await http.get(url);
//   print(response.body);
//   return allTestsFromJson(response.body);
// }

Future<LoginModel> insertApp(
    String full_name,
    String app_date,
    String street_num,
    String location_num,
    String land_num,
    String office_num,
    String tele_num,
    String app_type) async {
  String url = mainAPI +
      "insert_app.php?full_name=$full_name&app_date=$app_date&street_num=$street_num" +
      "&location_num=$location_num&land_num=$land_num&office_num=$office_num&tele_num=$tele_num&app_type=$app_type";
  print('asking for: $url');
  var encodedURL = Uri.encodeFull(url);
  try {
    final response = await http.get(encodedURL);
    print('response:' + response.body.toString());
    return loginModelFromJson(response.body);
  } catch (Exception) {
    return null;
  }
}

Future<Users> getUserDetails(String username) async {
  String url = mainAPI + "get_user_details.php?user_name=$username";
  print('asking for: $url');
  try {
    final response = await http.get(url);
    return usersFromJson(response.body);
  } catch (Exception) {
    return null;
  }
}

// Future<Orders> getCompanyApps(String office_num) async {
//   String url = mainAPI + "get_company_apps?office_num=$office_num";
//   var encodedURL = Uri.encodeFull(url);
//   print('asking for: $encodedURL');
//   try {
//     final response = await http.get(encodedURL);
//     return ordersFromJson(response.body);
//   } on SocketException catch (_) {
//     return null;
//   } catch (Exception) {
//     return null;
//   }
// }

Future<Orders> getMyApps(int sender, String parm) async {
  String url;
  if (sender == MyConstants.user) {
    url = mainAPI + "get_user_details.php?user_name=$parm";
  } else {
    url = mainAPI + "get_company_apps?office_num=$parm";
  }
  var encodedURL = Uri.encodeFull(url);
  print('asking for: $encodedURL');
  try {
    final response = await http.get(encodedURL);
    return ordersFromJson(response.body);
  } on SocketException catch (_) {
    return null;
  } catch (Exception) {
    return null;
  }
}

Future<Orders> getApps(String state) async {
  String url = mainAPI + "get_apps.php?app_state=$state";
  print('asking for: $url');
  try {
    final response = await http.get(url);
    return ordersFromJson(response.body);
  } on SocketException catch (_) {
    return null;
  } catch (Exception) {
    return null;
  }
}

Future<LoginModel> getLogin(String username, String password) async {
  String url = mainAPI + "login.php?username=$username&password=$password";
  print('asking for: $url');
  try {
    final response = await http.get(url);
    return loginModelFromJson(response.body);
    // if (response.statusCode == 200) {
    //   // If server returns an OK response, parse the JSON
    //   return loginModelFromJson(response.body);
    // } else {
    //   // If that response was not OK, throw an error.
    //   // throw Exception('Failed to load post');
    //   return null;
    // }
  } on SocketException catch (_) {
    return null;
  } catch (Exception) {
    return null;
  }
}

Future<LoginModel> inserUser(String username, String password, String full_name,
    String id_num, String phone) async {
//   var queryParameters = {
//   'user_name': username,
//   'password': password,
// };
// var uri = Uri.https('http://192.168.1.83', '/hebco-cbl/insert_user.php', queryParameters);
// full_name
//id_num
//phone

  String url = mainAPI +
      "insert_user.php?user_name=$username&password=$password&full_name=$full_name&id_num=$id_num&phone=$phone";
  print('asking for: $url');
  try {
    final response = await http.get(url);
    print('response:' + response.body.toString());
    return loginModelFromJson(response.body);
    // if (response.statusCode == 200) {
    //   // If server returns an OK response, parse the JSON
    //   return loginModelFromJson(response.body);
    // } else {
    //   // If that response was not OK, throw an error.
    //   // throw Exception('Failed to load post');
    //   return null;
    // }
  } on SocketException catch (_) {
    return null;
  } catch (Exception) {
    return null;
  }
}

// Future<http.Response> createPost(Test post) async{
//   final response = await http.post('$url',
//       headers: {
//         HttpHeaders.contentTypeHeader: 'application/json',
//         HttpHeaders.authorizationHeader : ''
//       },
//       body: testToJson(post)
//   );
//   return response;
// }

//Future<Post> createPost(Post post) async{
//  final response = await http.post('$url',
//      headers: {
//        HttpHeaders.contentTypeHeader: 'application/json'
//      },
//      body: postToJson(post)
//  );
//
//  return postFromJson(response.body);
//}
