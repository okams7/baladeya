import 'package:flutter/material.dart';
import 'package:baladeya/methods/app_colors.dart';

class ButtonIconWidget extends StatelessWidget {
  final String title;
  final callBack;
  final int sender;
  final Icon icon;
  ButtonIconWidget(this.title, this.sender,this.icon , this.callBack);

  @override
  Widget build(BuildContext context) {
    return _buttonWidget();
  }

  Widget _buttonWidget() {
    return Container(
      height: 60,
      // width: 200,
      margin: EdgeInsets.only(left: 16, right: 16),
      decoration: BoxDecoration(
          color: blueApp,
          border: new Border.all(
              color: blueAppDark, width: 2.5, style: BorderStyle.solid),
          borderRadius: BorderRadius.all(Radius.circular(20.0))),

      child: ClipRRect(
        borderRadius: BorderRadius.all(Radius.circular(18.0)),
        child: Material(
          // shape: CircleBorder(),
          clipBehavior: Clip.antiAlias,
          color: Colors.transparent,
          child: InkWell(
            // splashColor: Colors.orange,
            highlightColor: Colors.orange,
            onTap: () {
              if (callBack != null) {
                callBack(sender);
              }
            },
            child: Row(
              children: <Widget>[
                Expanded(
                    flex: 2,
                    child: Text(title,
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 18, color: Colors.white))),
                Expanded(flex: 1, child: icon),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
