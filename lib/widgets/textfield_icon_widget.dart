import 'package:flutter/material.dart';
import 'package:baladeya/methods/app_colors.dart';

// class TextFieldIconWidget extends StatefulWidget{
//   final String title;
//   final callBackText;
//   final int sender;
//   final Icon icon;
//   final myController = TextEditingController();
//   TextFieldIconWidget(this.title, this.sender, this.icon,this.callBackText);
//   @override
//   State<StatefulWidget> createState() {
//     return _TextFieldIconWidget(title, sender,icon,callBackText);
//   }
// }

class TextFieldIconWidget extends StatelessWidget {
  // FocusNode _focus = new FocusNode();
  final String title;
  final callBackText;
  final int sender;
  final Icon icon;
  final bool obsecure;
  // final myController = TextEditingController();
  TextFieldIconWidget(this.title, this.sender, this.icon, this.obsecure,this.callBackText);


  @override
  Widget build(BuildContext context) {
    return _buildWidget();
  }

  Widget _buildWidget() {
    return Container(
      height: 60,
      // width: 200,
      margin: EdgeInsets.only(left: 16, right: 16),
      decoration: BoxDecoration(
          color: yellowApp,
          border: new Border.all(
              color: Colors.white, width: 0, style: BorderStyle.solid),
          borderRadius: BorderRadius.all(Radius.circular(20.0))),

      child: Directionality(
        textDirection: TextDirection.rtl,
        child: ListTile(
          leading: icon,
          title: TextField(
            obscureText: obsecure,
            // controller: myController,
            onChanged: (value){
              print('ttt:$value');
              callBackText(sender, value);
            },
            // : callBackText(sender, myController.text),
            decoration: InputDecoration(
              fillColor: Colors.transparent,
              filled: true,
//                      hintStyle:  TextStyle(color: Colors.white70),
              hintText: title,
//                      labelText: S.of(context).label_business_title,
            ),
          ),
        ),
      ),
    );
  }
}
