import 'package:flutter/material.dart';
import 'package:baladeya/methods/app_colors.dart';

class ButtonWidget extends StatelessWidget {
  final String title;
  final callBack;
  final int sender;
  ButtonWidget(this.title, this.sender,this.callBack);

  @override
  Widget build(BuildContext context) {
    return _buttonWidget();
  }

  Widget _buttonWidget() {
    return Container(
      height: 60,
      // width: 200,
      margin: EdgeInsets.only(left: 16, right: 16),
      decoration: BoxDecoration(
          color: yellowApp,
          border: new Border.all(
              color: blueAppDark, width: 2.5, style: BorderStyle.solid),
          borderRadius: BorderRadius.all(Radius.circular(20.0))),

      child: ClipRRect(
        borderRadius: BorderRadius.all(Radius.circular(18.0)),
        child: Material(
          // shape: CircleBorder(),
          clipBehavior: Clip.antiAlias,
          color: Colors.transparent,
          child: InkWell(
              // splashColor: Colors.orange,
              highlightColor: Colors.orange,
              onTap: () {
                if (callBack != null) {
                  callBack(sender);
                }
              },
              child:
              Center(child: Text(title,
                          textAlign: TextAlign.center,
                          style:
                              TextStyle(fontSize: 18, color: Colors.black54))),), 
              // child: Row(
              //   children: <Widget>[
              //     Expanded(
              //         flex: 2,
              //         child: Text(title,
              //             textAlign: TextAlign.center,
              //             style:
              //                 TextStyle(fontSize: 18, color: Colors.black54))),
              //     Expanded(flex: 1, child: Icon(Icons.access_alarm)),
              //   ],
              // ),
              // ),
        ),
      ),
    );
  }
}
