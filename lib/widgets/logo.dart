import 'package:flutter/material.dart';

class LogoWidget extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Container(
              width: 150,
              height: 150,
              decoration: new BoxDecoration(
                image: new DecorationImage(
                  image: new AssetImage('assets/startup_logo.jpg'),
                  fit: BoxFit.cover,
                ),
              ),
            );
  }
}