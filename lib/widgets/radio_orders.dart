import 'package:flutter/material.dart';

class RadioOrders extends StatefulWidget {
  final radioCallBack;
  RadioOrders(this.radioCallBack);

  @override
  State<StatefulWidget> createState() {
    return _RadioOrdersState(radioCallBack);
  }
}

class _RadioOrdersState extends State<RadioOrders> {
  final radioCallBack;
  _RadioOrdersState(this.radioCallBack);
  int _radioValue1 = 0;
  void _handleRadioValueChange1(int value) {
    setState(() {
      _radioValue1 = value;
    });
    radioCallBack(value);
  }

  @override
  Widget build(BuildContext context) {
    return 
    Container(
      margin: EdgeInsets.only(right: 16, left: 16, top: 16 ,bottom: 16),
      child: Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: <Widget>[
        Text(
          'عرض الطلبات',
          style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
        ),
        // Divider(height: 5.0, color: Colors.black),
        Row(mainAxisAlignment: MainAxisAlignment.end, children: <Widget>[
          Expanded(
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Radio(
                      value: 0,
                      groupValue: _radioValue1,
                      onChanged: _handleRadioValueChange1,
                    ),
                    Text(
                      'الجميع',
                      style: TextStyle(fontSize: 16.0),
                    ),
                  ],
                ),
                SizedBox(
                  height: 16,
                ),
                Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Radio(
                        value: 1,
                        groupValue: _radioValue1,
                        onChanged: _handleRadioValueChange1,
                      ),
                      Text(
                        'المقبولة',
                        style: TextStyle(
                          fontSize: 16.0,
                        ),
                      ),
                    ]),
              ],
            ),
          ),
          SizedBox(width: 26,),
          Expanded(
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Radio(
                      value: 2,
                      groupValue: _radioValue1,
                      onChanged: _handleRadioValueChange1,
                    ),
                    Text(
                      'المرفوضة',
                      style: TextStyle(fontSize: 16.0),
                    ),
                  ],
                ),
                SizedBox(
                  height: 16,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Radio(
                      value: 3,
                      groupValue: _radioValue1,
                      onChanged: _handleRadioValueChange1,
                    ),
                    Text(
                      'قيد الانتظار',
                      style: TextStyle(fontSize: 16.0),
                    ),
                  ],
                ),
              ],
            ),
          )
        ]),
      ],
    ),);
  }
}
