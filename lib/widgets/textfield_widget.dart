import 'package:flutter/material.dart';
import 'package:baladeya/methods/app_colors.dart';

class TextFieldWidget extends StatelessWidget {
  final callBackText;
  final String hint;
  final int sender;
  final String title;
  final controller = TextEditingController();
  TextFieldWidget(this.hint, this.sender, this.callBackText, {this.title});

  @override
  Widget build(BuildContext context) {
    return _buildWidget();
  }

  Widget _buildWidget() {
    if (title != null && title.length > 0) {
      controller.text = title;
    }

    return Container(
      height: 60,
      // width: 200,
      margin: EdgeInsets.only(left: 16, right: 16),
      decoration: BoxDecoration(
          color: yellowApp,
          border: new Border.all(
              color: Colors.white, width: 0, style: BorderStyle.solid),
          borderRadius: BorderRadius.all(Radius.circular(20.0))),
      child: Container(
        margin: EdgeInsets.only(left: 16, right: 16),
        child: Directionality(
          textDirection: TextDirection.rtl,
          child: TextField(
            controller: controller,
            onChanged: (value) {
              callBackText(sender, value);
            },
            decoration: InputDecoration(
              labelText: hint,
              fillColor: Colors.white,
            ),
//             decoration: InputDecoration(
//               fillColor: Colors.transparent,
//               // filled: true,
// //                      hintStyle:  TextStyle(color: Colors.white70),
//               hintText: hint,
// //                      labelText: S.of(context).label_business_title,
//             ),
          ),
        ),
      ),
    );
  }
}
