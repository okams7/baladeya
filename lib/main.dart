import 'package:flutter/material.dart';
import 'package:baladeya/pages/intro_page.dart';
import 'package:baladeya/pages/login_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {


//(Mac)
// /Users/okams/Library/Android/sdk/emulator/emulator -avd Pixel_XL_API_P -netdelay none -netspeed full
//(Windows)
  //in bash
  //cd C:/Users/HP/AppData/Local/Android/Sdk/emulator
  //./emulator -avd Pixel_XL_API_28 -netdelay none -netspeed full

  //in powershell
  //cd C:\Users\HP\AppData\Local\Android\Sdk\emulator
  //.\emulator -avd Pixel_XL_API_28 -netdelay none -netspeed full
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      routes: <String, WidgetBuilder>{
        '/LoginPage': (BuildContext context) => LoginPage(),
        // '/BkgPage': (BuildContext context) => BKGPage(),
        // '/SigningProcess': (BuildContext context) => SigningProcess(),
      },
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: IntroPage(title: 'Flutter Demo Home Page'),
    );
  }
}