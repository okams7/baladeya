import 'dart:async';

import 'package:baladeya/methods/api_services.dart';
import 'package:baladeya/models/users.dart';
import 'package:flutter/material.dart';
import 'package:baladeya/methods/constants.dart';
import 'package:baladeya/widgets/button_text.dart';
import 'package:baladeya/widgets/textfield_widget.dart';
import 'package:baladeya/pages/new_order_page.dart';
import 'package:baladeya/pages/show_orders_page.dart';
import 'package:baladeya/methods/app_colors.dart';
import 'package:shared_preferences/shared_preferences.dart';

class EngOfficeMainPage extends StatefulWidget {
  @override
  _EngOfficeMainPage createState() {
    return _EngOfficeMainPage();
  }
}

class _EngOfficeMainPage extends State<EngOfficeMainPage> {
  User user;
  String email;
  String company;
  String phone;
  BuildContext _scaffoldContext;

  @override
  void initState() {
    getSharedPreferences();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Widget body = Center(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            TextFieldWidget(
              "البريد الالكتروني",
              MyConstants.employee,
              callBack,
              title: email,
            ),
            SizedBox(
              height: 16,
            ),
            TextFieldWidget(
              "اسم الشركة",
              MyConstants.employee,
              callBack,
              title: company,
            ),
            SizedBox(
              height: 16,
            ),
            TextFieldWidget(
              "رقم الهاتف",
              MyConstants.employee,
              callBack,
              title: phone,
            ),
            SizedBox(
              height: 16,
            ),
            _buttonsRow(),
          ],
          //
        ),
      ),
    );
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'الصفحة الرئيسية',
          style: TextStyle(fontSize: 16),
        ),
        iconTheme: IconThemeData(color: blueApp),
        textTheme: TextTheme(
          headline: TextStyle(color: blueApp),
          title: TextStyle(color: blueApp),
        ),
        backgroundColor: yellowApp,
      ),
      backgroundColor: blueApp,
      body: Builder(builder: (BuildContext context) {
        _scaffoldContext = context;
        return body;
      }),
    );
  }

  void getRemoteData(String username) async {
    loadingBar(true);
    final model = await getUserDetails(username);
    loadingBar(false);
    //
    if (model != null) {
      print('ZLoginModel:${model.res.toString()}');
      if (model.res == 1) {
        print('Successful');
        user = model.data[0];
        saveToSharedPreferences(user.id);
        setState(() {
          email = user.userName;
          company = user.fullName;
          phone = user.phone;
        });
        // showSB('عدد الطلبات : ${model.data.length}');
        // Scaffold.of(_scaffoldContext).showSnackBar(
        //     SnackBar(content: Text('عدد الطلبات : ${model.data.length}')));
      } else {
        print('Unsuccessful');
        showSB('خطأ');
        // Scaffold.of(_scaffoldContext)
        //     .showSnackBar(SnackBar(content: Text('خطأ')));
      }
    } else {
      print('ZLoginModel:null');
      showSB('حدث خطأ ما.. ');
      // Scaffold.of(_scaffoldContext)
      //     .showSnackBar(SnackBar(content: Text('حدث خطأ ما.. ')));
    }
  }

  void loadingBar(bool show) async {
    await Future.delayed(Duration(milliseconds: 50));
    if (show) {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return Center(
            child: Container(
              height: 100,
              width: 100,
              margin: EdgeInsets.only(left: 16, right: 16),
              decoration: BoxDecoration(
                  color: Colors.black.withOpacity(0.5),
                  // border: new Border.all(
                  //     color: Colors.black, width: 2.5, style: BorderStyle.solid),
                  borderRadius: BorderRadius.all(Radius.circular(20.0))),
              child: Center(
                child: CircularProgressIndicator(),
              ),
            ),
          );
        },
      );
    } else {
      Navigator.pop(context);
    }
  }

  Widget _buttonsRow() {
    return Row(
      children: <Widget>[
        Expanded(
          child: ButtonWidget("تحديث", MyConstants.updateRequest, callBack),
        ),
        Expanded(
          child: ButtonWidget("طلب جديد", MyConstants.newOrder, callBack),
        ),
        Expanded(
          child: ButtonWidget("عرض الطلبات", MyConstants.showOrders, callBack),
        ),
      ],
    );
  }

  void callBack(int sender) {
    switch (sender) {
      case MyConstants.newOrder:
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => NewOrderPage(),
          ),
        );
        break;
      case MyConstants.showOrders:
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => ShowOrdersPage(sender: MyConstants.engOffice,),
          ),
        );
        break;
      default:
    }
  }

  void showSB(String msg) {
    Scaffold.of(_scaffoldContext)
      ..removeCurrentSnackBar()
      ..showSnackBar(SnackBar(content: Text(msg)));
  }

  void saveToSharedPreferences(String office_num) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(MyConstants.s_office_num, office_num);
  }

  void getSharedPreferences() async {
    final prefs = await SharedPreferences.getInstance();
    String username = prefs.getString(MyConstants.s_username) ?? '';
    getRemoteData(username);
  }
}
