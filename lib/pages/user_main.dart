import 'dart:async';

import 'package:baladeya/methods/api_services.dart';
import 'package:baladeya/models/users.dart';
import 'package:flutter/material.dart';
import 'package:baladeya/methods/constants.dart';
import 'package:baladeya/widgets/button_text.dart';
import 'package:baladeya/widgets/textfield_widget.dart';
import 'package:baladeya/pages/new_order_page.dart';
import 'package:baladeya/pages/show_orders_page.dart';
import 'package:baladeya/methods/app_colors.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserMainPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _UserMainPage();
  }
}

class _UserMainPage extends State<UserMainPage> {
  User user;
  String _username;
  String _fullName;
  String _id_num;
  String _phone;
  BuildContext _scaffoldContext;

  @override
  void initState() {
    getSharedPreferences();
    super.initState();
  }

  void getRemoteData(String username) async {
    loadingBar(true);
    final model = await getUserDetails(username);
    loadingBar(false);
    //
    if (model != null) {
      print('ZLoginModel:${model.res.toString()}');
      if (model.res == 1) {
        print('Successful');
        user = model.data[0];
        saveToSharedPreferences(user.fullName);
        setState(() {
          _username = user.userName;
          _fullName = user.fullName;
          _id_num = user.idNum;
          _phone = user.phone;
        });
        // showSB('عدد الطلبات : ${model.data.length}');
        // Scaffold.of(_scaffoldContext).showSnackBar(
        //     SnackBar(content: Text('عدد الطلبات : ${model.data.length}')));
      } else {
        print('Unsuccessful');
        showSB('خطأ');
        // Scaffold.of(_scaffoldContext)
        //     .showSnackBar(SnackBar(content: Text('خطأ')));
      }
    } else {
      print('ZLoginModel:null');
      showSB('حدث خطأ ما.. ');
      // Scaffold.of(_scaffoldContext)
      //     .showSnackBar(SnackBar(content: Text('حدث خطأ ما.. ')));
    }
  }

  void loadingBar(bool show) async {
    await Future.delayed(Duration(milliseconds: 50));
    if (show) {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return Center(
            child: Container(
              height: 100,
              width: 100,
              margin: EdgeInsets.only(left: 16, right: 16),
              decoration: BoxDecoration(
                  color: Colors.black.withOpacity(0.5),
                  // border: new Border.all(
                  //     color: Colors.black, width: 2.5, style: BorderStyle.solid),
                  borderRadius: BorderRadius.all(Radius.circular(20.0))),
              child: Center(
                child: CircularProgressIndicator(),
              ),
            ),
          );
        },
      );
    } else {
      Navigator.pop(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    Widget body = Center(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            TextFieldWidget(
              "اسم المستخدم",
              MyConstants.employee,
              callBack,
              title: _username,
            ),
            SizedBox(
              height: 16,
            ),
            TextFieldWidget(
              "الاسم الرباعي",
              MyConstants.employee,
              callBack,
              title: _fullName,
            ),
            SizedBox(
              height: 16,
            ),
            TextFieldWidget(
              "رقم الهوية",
              MyConstants.employee,
              callBack,
              title: _id_num,
            ),
            SizedBox(
              height: 16,
            ),
            TextFieldWidget(
              "رقم الجوال",
              MyConstants.employee,
              callBack,
              title: _phone,
            ),
            SizedBox(
              height: 16,
            ),
            _buttonsRow(),
          ],

          //
        ),
      ),
    );

    return Scaffold(
      appBar: AppBar(
        title: Text(
          'الصفحة الرئيسية',
          style: TextStyle(fontSize: 16),
        ),
        iconTheme: IconThemeData(color: blueApp),
        textTheme: TextTheme(
          headline: TextStyle(color: blueApp),
          title: TextStyle(color: blueApp),
        ),
        backgroundColor: yellowApp,
      ),
      backgroundColor: blueApp,
      body: Builder(builder: (BuildContext context) {
        _scaffoldContext = context;
        return body;
      }),
    );
  }

  Widget _buttonsRow() {
    return Row(
      children: <Widget>[
        Expanded(
          child: ButtonWidget("تحديث", MyConstants.updateRequest, callBack),
        ),
        Expanded(
          child: ButtonWidget("طلب جديد", MyConstants.newOrder, callBack),
        ),
        Expanded(
          child: ButtonWidget("عرض الطلبات", MyConstants.showOrders, callBack),
        ),
      ],
    );
  }

  void callBack(int sender) {
    switch (sender) {
      case MyConstants.newOrder:
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => NewOrderPage(),
          ),
        );
        break;
      case MyConstants.showOrders:
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => ShowOrdersPage(sender: MyConstants.user),
          ),
        );
        break;
      default:
    }
  }

  void showSB(String msg) {
    Scaffold.of(_scaffoldContext)
      ..removeCurrentSnackBar()
      ..showSnackBar(SnackBar(content: Text(msg)));
  }

  void saveToSharedPreferences(String fullName) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(MyConstants.s_fullName, fullName);
  }

  void getSharedPreferences() async {
    final prefs = await SharedPreferences.getInstance();
    String username = prefs.getString(MyConstants.s_username) ?? '';
    getRemoteData(username);
  }
}
