import 'package:flutter/material.dart';
// import 'package:baladeya/methods/constants.dart';
// import 'package:baladeya/widgets/button_text.dart';
import 'package:baladeya/widgets/radio_orders.dart';
// import 'package:baladeya/models/orders.dart';
import 'package:baladeya/methods/app_colors.dart';
import 'package:baladeya/methods/api_services.dart';
import 'package:baladeya/models/orders_m.dart';
import 'package:date_format/date_format.dart';
import 'dart:async';

class ControlOrdersPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _ControlOrdersPage();
  }
}

class _ControlOrdersPage extends State<ControlOrdersPage> {
  //
  List<Order> items = [];
  BuildContext _scaffoldContext;
  DateTime selectedDate = DateTime.now();
  String choosenDate = "";

  @override
  void initState() {
    // items = [
    //   Order(id: "1", fullName: "asasa", appDate: "asdasdads"),
    //   Order(id: "2", fullName: "asasa", appDate: "12/3/2019"),
    //   Order(id: "3", fullName: "dfsdf", appDate: "23/1/2018"),
    //   Order(id: "4", fullName: "rege vdv", appDate: "22/1/2019"),
    // ];

    ordersInfo("All");
    setState(() {
      choosenDate = getDateInString();
    });
    super.initState();
  }

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
        choosenDate = getDateInString();
      });
  }

  String getDateInString() {
    String date = formatDate(selectedDate, [dd, '/', mm, '/', yyyy]);
    return date;
  }

  void ordersInfo(String state) async {
    loadingBar(true);
    final model = await getApps(state);
    loadingBar(false);
    //
    if (model != null) {
      print('ZLoginModel:${model.res.toString()}');
      if (model.res == 1) {
        print('Successful');
        setState(() {
          items = model.data;
        });
        showSB('عدد الطلبات : ${model.data.length}');
        // Scaffold.of(_scaffoldContext).showSnackBar(
        //     SnackBar(content: Text('عدد الطلبات : ${model.data.length}')));
      } else {
        print('Unsuccessful');
        showSB('خطأ');
        // Scaffold.of(_scaffoldContext)
        //     .showSnackBar(SnackBar(content: Text('خطأ')));
      }
    } else {
      print('ZLoginModel:null');
      setState(() {
        items = [];
      });
      showSB('حدث خطأ ما.. ');
      // Scaffold.of(_scaffoldContext)
      //     .showSnackBar(SnackBar(content: Text('حدث خطأ ما.. ')));
    }
  }

  void loadingBar(bool show) async {
    await Future.delayed(Duration(milliseconds: 50));
    if (show) {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return Center(
            child: Container(
              height: 100,
              width: 100,
              margin: EdgeInsets.only(left: 16, right: 16),
              decoration: BoxDecoration(
                  color: Colors.black.withOpacity(0.5),
                  // border: new Border.all(
                  //     color: Colors.black, width: 2.5, style: BorderStyle.solid),
                  borderRadius: BorderRadius.all(Radius.circular(20.0))),
              child: Center(
                child: CircularProgressIndicator(),
              ),
            ),
          );
        },
      );
    } else {
      Navigator.pop(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    Widget body = Stack(
      children: <Widget>[
        Column(
          children: <Widget>[
            RadioOrders(radioCallBack),
            new Expanded(
              child: new ListView.builder(
                  itemCount: items.length,
                  itemBuilder: (BuildContext ctxt, int index) {
                    final item = items[index];
                    return ListTile(
                      leading: Text(item.id),
                      title: Text(item.fullName),
                      subtitle: Text(item.appDate),
                    );
                  }),
            ),
          ],
        ),
        Container(
          margin: EdgeInsets.all(8),
          child: RaisedButton(
            child: Text(choosenDate),
            onPressed: () {
              _selectDate(context);
            },
          ),
        ),
      ],
    );

    return Scaffold(
      appBar: AppBar(
        title: Text(
          'التحكم بالطلبات',
          style: TextStyle(fontSize: 16),
        ),
        iconTheme: IconThemeData(color: blueApp),
        textTheme: TextTheme(
          headline: TextStyle(color: blueApp),
          title: TextStyle(color: blueApp),
        ),
        backgroundColor: yellowApp,
      ),
      // backgroundColor: Colors.blueGrey,
      body: Builder(builder: (BuildContext context) {
        _scaffoldContext = context;
        return body;
      }),
    );
  }

  void callBack(int sender) {}
  void radioCallBack(int choice) {
    print('radio $choice is choosed');
    switch (choice) {
      case 0:
        ordersInfo("All");
        break;
      case 1:
        ordersInfo("Rejected");
        break;
      case 2:
        ordersInfo("Accepted");
        break;
      case 3:
        ordersInfo("OnProgress");
        break;
      default:
    }
  }


void showSB(String msg) {
    Scaffold.of(_scaffoldContext)
      ..removeCurrentSnackBar()
      ..showSnackBar(SnackBar(content: Text(msg)));
  }

}
