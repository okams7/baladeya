import 'package:flutter/material.dart';

import 'package:baladeya/widgets/button_text.dart';
import 'package:baladeya/widgets/textfield_widget.dart';
import 'package:baladeya/methods/constants.dart';
// import 'package:baladeya/pages/employee_main.dart';
// import 'package:baladeya/pages/eng_office_main.dart';
// import 'package:baladeya/pages/user_main.dart';
import 'package:baladeya/methods/app_colors.dart';

class ControlPricesPage extends StatefulWidget {
  ControlPricesPage({this.sender});
  final int sender;

  @override
  _ControlPricesPage createState() => _ControlPricesPage(sender: sender);
}

class _ControlPricesPage extends State<ControlPricesPage> {
  _ControlPricesPage({this.sender});
  final int sender;
  String priceTitle = 'السعر';
  String editedByTitle ='عدل بواسطة';
  String modifyTitle ='تاريخ التعديل';
  String updateTitle ='تحديث';

  @override
  void initState() {
    print('sender: $sender');
    // double height = MediaQuery.of(context).size.height;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'التحكم بالاسعار',
          style: TextStyle(fontSize: 16),
        ),
        iconTheme: IconThemeData(color: blueApp),
        textTheme: TextTheme(
          headline: TextStyle(color: blueApp),
          title: TextStyle(color: blueApp),
        ),
        backgroundColor: yellowApp,
      ),
      backgroundColor: blueApp,
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                height: 8,
              ),
              Text("طلب جديد", style: TextStyle(fontSize: 18, color: Colors.white),),
              SizedBox(height: 8,),
              TextFieldWidget(priceTitle, MyConstants.employee, callBack),
              SizedBox(
                height: 8,
              ),
              TextFieldWidget(editedByTitle, MyConstants.employee, callBack),
              SizedBox(
                height: 8,
              ),
              TextFieldWidget(modifyTitle, MyConstants.engOffice, callBack),
              SizedBox(
                height: 16,
              ),
              ButtonWidget(updateTitle, MyConstants.employee, callBack),

              SizedBox(height: 26,),
              Text("طلب تجديد", style: TextStyle(fontSize: 18, color: Colors.white),),
              SizedBox(height: 8,),
              TextFieldWidget(priceTitle, MyConstants.employee, callBack),
              SizedBox(
                height: 8,
              ),
              TextFieldWidget(editedByTitle, MyConstants.employee, callBack),
              SizedBox(
                height: 8,
              ),
              TextFieldWidget(modifyTitle, MyConstants.engOffice, callBack),
              SizedBox(
                height: 16,
              ),
              ButtonWidget(updateTitle, MyConstants.employee, callBack),

              SizedBox(height: 26,),
              Text("طلب ملحق", style: TextStyle(fontSize: 18, color: Colors.white),),
              SizedBox(height: 8,),
              TextFieldWidget(priceTitle, MyConstants.employee, callBack),
              SizedBox(
                height: 8,
              ),
              TextFieldWidget(editedByTitle, MyConstants.employee, callBack),
              SizedBox(
                height: 8,
              ),
              TextFieldWidget(modifyTitle, MyConstants.engOffice, callBack),
              SizedBox(
                height: 16,
              ),
              ButtonWidget(updateTitle, MyConstants.employee, callBack),
              SizedBox(height: 26,),
            ],
          ),
        ),
      ),
    );
  }

  void callBack(int sender) {
  }
}
