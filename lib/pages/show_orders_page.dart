import 'package:baladeya/methods/api_services.dart';
import 'package:baladeya/methods/constants.dart';
import 'package:flutter/material.dart';
import 'package:baladeya/methods/app_colors.dart';
import 'package:baladeya/models/orders_m.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ShowOrdersPage extends StatefulWidget {
  final int sender;
  ShowOrdersPage({this.sender});

  @override
  State<StatefulWidget> createState() {
    return _ShowOrdersPage(sender: sender);
  }
}

class _ShowOrdersPage extends State<ShowOrdersPage> {
  int sender;
  _ShowOrdersPage({this.sender});
  BuildContext _scaffoldContext;
  List<Order> items = [];

  @override
  void initState() {
    // items = [
    //   Orders(id: 1, name: "asasa", date: "12/3/2019"),
    //   Orders(id: 2, name: "dfsdf", date: "23/1/2018"),
    //   Orders(id: 3, name: "rege vdv", date: "22/1/2019"),
    // ];
    getSharedPreferences();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Widget body = ListView.builder(
        itemCount: items.length,
        itemBuilder: (BuildContext ctxt, int index) {
          final item = items[index];
          return ListTile(
            leading: Text(item.id.toString()),
            title: Text(item.fullName),
            subtitle: Text(item.appDate),
          );
        });

    return Scaffold(
      appBar: AppBar(
        title: Text(
          'الطلبات',
          style: TextStyle(fontSize: 16),
        ),
        iconTheme: IconThemeData(color: blueApp),
        textTheme: TextTheme(
          headline: TextStyle(color: blueApp),
          title: TextStyle(color: blueApp),
        ),
        backgroundColor: yellowApp,
      ),
      // backgroundColor: Colors.blueGrey,
      body: Builder(builder: (BuildContext context) {
        _scaffoldContext = context;
        return body;
      }),
    );
  }

  void callBack(int sender) {}
  void radioCallBack(int choice) {
    print('radio $choice is choosed');
  }

  void getRemoteData(String parm) async {
    loadingBar(true);
    final model = await getMyApps(sender,parm);
    loadingBar(false);
    //
    if (model != null) {
      print('ZLoginModel:${model.res.toString()}');
      if (model.res == 1) {
        print('Successful');
        setState(() {
          items = model.data;
        });
        showSB('عدد الطلبات : ${model.data.length}');
        // Scaffold.of(_scaffoldContext).showSnackBar(
        //     SnackBar(content: Text('عدد الطلبات : ${model.data.length}')));
      } else {
        print('Unsuccessful');
        showSB('خطأ');
        // Scaffold.of(_scaffoldContext)
        //     .showSnackBar(SnackBar(content: Text('خطأ')));
      }
    } else {
      print('ZLoginModel:null');
      setState(() {
        items = [];
      });
      showSB('حدث خطأ ما.. ');
      // Scaffold.of(_scaffoldContext)
      //     .showSnackBar(SnackBar(content: Text('حدث خطأ ما.. ')));
    }
  }

  void loadingBar(bool show) async {
    await Future.delayed(Duration(milliseconds: 50));
    if (show) {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return Center(
            child: Container(
              height: 100,
              width: 100,
              margin: EdgeInsets.only(left: 16, right: 16),
              decoration: BoxDecoration(
                  color: Colors.black.withOpacity(0.5),
                  // border: new Border.all(
                  //     color: Colors.black, width: 2.5, style: BorderStyle.solid),
                  borderRadius: BorderRadius.all(Radius.circular(20.0))),
              child: Center(
                child: CircularProgressIndicator(),
              ),
            ),
          );
        },
      );
    } else {
      Navigator.pop(context);
    }
  }

  void showSB(String msg) {
    Scaffold.of(_scaffoldContext)
      ..removeCurrentSnackBar()
      ..showSnackBar(SnackBar(content: Text(msg)));
  }

  void getSharedPreferences() async {
    final prefs = await SharedPreferences.getInstance();
    String text;
    if (sender ==MyConstants.user) {
      text = MyConstants.s_fullName;
    }
    else{
      text = MyConstants.s_office_num;
    }
    String param = prefs.getString(text) ?? '';
    getRemoteData(param);
  }
}
