import 'package:flutter/material.dart';
import 'dart:async';

import 'package:baladeya/widgets/logo.dart';
import 'package:baladeya/widgets/button_text_icon.dart';
import 'package:baladeya/methods/constants.dart';
import 'package:baladeya/pages/login_page.dart';
import 'package:baladeya/models/test_model.dart';
import 'package:baladeya/methods/test_services.dart';
import 'package:baladeya/methods/app_colors.dart';


// class Post {
//   final int userId;
//   final int id;
//   final String title;
//   final String body;

//   Post({this.userId, this.id, this.title, this.body});

//   factory Post.fromJson(Map<String, dynamic> json) {
//     return Post(
//       userId: json['userId'],
//       id: json['id'],
//       title: json['title'],
//       body: json['body'],
//     );
//   }
// }

class IntroPage extends StatefulWidget {
  IntroPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _IntroPageState createState() => _IntroPageState();
}

class _IntroPageState extends State<IntroPage> {

@override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    print('IntroPage build');
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(color: yellowApp),
          Align(
            alignment: FractionalOffset.bottomCenter,
            child: Container(
              height: 200,
              decoration: new BoxDecoration(
                image: new DecorationImage(
                  image: new AssetImage('assets/bottom.jpg'),
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Center(child: LogoWidget(),),
              SizedBox(height: 40,),
              ButtonIconWidget('موظف بلدية',MyConstants.employee, Icon(Icons.supervisor_account, color: Colors.white,),callBack),
              SizedBox(height: 8,),
              ButtonIconWidget('مكتب هندسي',MyConstants.engOffice,Icon(Icons.streetview,color: Colors.white,),callBack),
              SizedBox(height: 8,),
              ButtonIconWidget('مستخدم',MyConstants.user,Icon(Icons.person,color: Colors.white,),callBack),
              SizedBox(height: 100,),
          ],),
//3332342342424242342424242424342345547
        //   FutureBuilder<List<Test>>(
        //     future: getAllTest(),
        //     builder: (context, snapshot) {
        //       if(snapshot.connectionState == ConnectionState.done) {
        //         if(snapshot.hasError){
        //           return Text("Error");
        //         }
        //         return Text('Title from Post JSON : ${snapshot.data[0].title}');
        //       }
        //       else
        //         return CircularProgressIndicator();
        //     }
        // )
        ],
      ),
    );
  }

  void callBack(int sender){
    print('sender $sender');
    navigationPage(sender);
    // switch (sender) {
    //   case MyConstants.Employee:
        
    //     break;
    //     case MyConstants.EngOffice:
        
    //     break;
    //     case MyConstants.User:
        
    //     break;
    //   default:
    // }
  }

  void navigationPage(int sender) {
    Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => LoginPage(sender: sender),
          ),
        );
    // Navigator.of(context).pushReplacementNamed('/LoginPage');
  }
}
