import 'package:flutter/material.dart';

import 'package:baladeya/widgets/button_text.dart';
import 'package:baladeya/widgets/textfield_widget.dart';
import 'package:baladeya/methods/constants.dart';
import 'package:baladeya/pages/employee_main.dart';
import 'package:baladeya/pages/eng_office_main.dart';
import 'package:baladeya/pages/user_main.dart';
import 'package:baladeya/methods/api_services.dart';
import 'package:baladeya/methods/app_colors.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SignUpPage extends StatefulWidget {
  SignUpPage({this.sender});
  final int sender;

  @override
  _SignUpPageState createState() => _SignUpPageState(sender: sender);
}

class _SignUpPageState extends State<SignUpPage> {
  BuildContext _scaffoldContext;
  _SignUpPageState({this.sender});
  final int sender;
  String userTitle;
  String passTitle;
  String loginTitle;
  String signupTitle;
  String username = "";
  String password = "";
  String full_name = "";
  String id_num = "";
  String phone = "";
  List<Widget> senderWidgets;

  @override
  void initState() {
    print('sender: $sender');
    // double height = MediaQuery.of(context).size.height;
    switch (sender) {
      case MyConstants.employee:
        userTitle = "الرقم الوظيفي";
        passTitle = "كلمة السر";
        loginTitle = "تسجيل";
        senderWidgets = [
          TextFieldWidget(userTitle, MyConstants.username, callBackText),
          SizedBox(
            height: 8,
          ),
          TextFieldWidget(passTitle, MyConstants.password, callBackText),
          SizedBox(
            height: 16,
          ),
          ButtonWidget(loginTitle, MyConstants.employee, callBack),
        ];
        break;
      case MyConstants.engOffice:
        userTitle = "الايميل";
        passTitle = "كلمة المرور";
        String fullName = "اسم المكتب";
        String phoneNumber = "الهاتف";
        loginTitle = "تسجيل";
        senderWidgets = [
          TextFieldWidget(userTitle, MyConstants.username, callBackText),
          SizedBox(
            height: 8,
          ),
          TextFieldWidget(passTitle, MyConstants.password, callBackText),
          SizedBox(
            height: 16,
          ),
          TextFieldWidget(fullName, MyConstants.full_name, callBackText),
          SizedBox(
            height: 16,
          ),
          TextFieldWidget(phoneNumber, MyConstants.phone, callBackText),
          SizedBox(
            height: 16,
          ),
          ButtonWidget(loginTitle, MyConstants.engOffice, callBack),
        ];
        break;
      case MyConstants.user:
        userTitle = "اسم المستخدم";
        passTitle = "كلمة السر";
        String fullName = "الاسم الرباعي";
        String idNumber = "رقم الهوية";
        String phoneNumber = "رقم الجوال";
        loginTitle = "تسجيل";
        senderWidgets = [
          TextFieldWidget(userTitle, MyConstants.username, callBackText),
          SizedBox(
            height: 8,
          ),
          TextFieldWidget(passTitle, MyConstants.password, callBackText),
          SizedBox(
            height: 16,
          ),
          TextFieldWidget(fullName, MyConstants.full_name, callBackText),
          SizedBox(
            height: 16,
          ),
          TextFieldWidget(idNumber, MyConstants.id_num, callBackText),
          SizedBox(
            height: 16,
          ),
          TextFieldWidget(phoneNumber, MyConstants.phone, callBackText),
          SizedBox(
            height: 16,
          ),
          ButtonWidget(loginTitle, MyConstants.user, callBack),
        ];
        break;
      default:
        userTitle = "خطأ";
        passTitle = "خطأ";
        loginTitle = "خطأ";
        signupTitle = "خطأ";
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Widget body = Center(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: senderWidgets,
        ),
      ),
    );

    return Scaffold(
      appBar: AppBar(
        title: Text(
          'تسجيل مستخدم جديد',
          style: TextStyle(fontSize: 16),
        ),
        iconTheme: IconThemeData(color: blueApp),
        textTheme: TextTheme(
          headline: TextStyle(color: blueApp),
          title: TextStyle(color: blueApp),
        ),
        backgroundColor: yellowApp,
      ),
      backgroundColor: blueApp,
      body: new Builder(builder: (BuildContext context) {
        _scaffoldContext = context;
        return body;
      }),
    );
  }

  void signInfo(int sender) async {
    final loginModel =
        await inserUser(username, password, full_name, id_num, phone);
    Navigator.pop(context);
    if (loginModel != null) {
      print('ZLoginModel:${loginModel.id}');
      if (loginModel.res == 1) {
        print('Successful');
        saveToSharedPreferences(username);
        navigatorGoTo(sender);
      } else {
        print('Unsuccessful');
        showSB('اسم المستخدم او كلمة المرور غير صحيحة!');
        // Scaffold.of(_scaffoldContext).showSnackBar(
        //     SnackBar(content: Text()));
      }
    } else {
      print('ZLoginModel:null');
      showSB('حدث خطأ ما.. تأكد من اتصالك بالانترنت');
      // Scaffold.of(_scaffoldContext).showSnackBar(
      //     SnackBar(content: Text()));
    }
  }

  void loadingBar() {
    // if (isLoading) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return Center(
          child: Container(
            height: 100,
            width: 100,
            margin: EdgeInsets.only(left: 16, right: 16),
            decoration: BoxDecoration(
                color: Colors.black.withOpacity(0.5),
                // border: new Border.all(
                //     color: Colors.black, width: 2.5, style: BorderStyle.solid),
                borderRadius: BorderRadius.all(Radius.circular(20.0))),
            child: Center(
              child: CircularProgressIndicator(),
            ),
          ),
        );
      },
    );
  }

  void callBackText(int sender, String text) {
    print('test: $text');
    switch (sender) {
      case MyConstants.username:
        username = text;
        break;
      case MyConstants.password:
        password = text;
        break;
      case MyConstants.full_name:
        full_name = text;
        break;
      case MyConstants.id_num:
        id_num = text;
        break;
      case MyConstants.phone:
        phone = text;
        break;
      default:
    }
  }

  void callBack(int sender) {
    switch (sender) {
      case MyConstants.employee:
        if (username.length > 0 && password.length > 0) {
          loadingBar();
          signInfo(sender);
        } else {
          showSB('يجب كتابة الحقول المطلوبة!');
          // Scaffold.of(_scaffoldContext).showSnackBar(
          //     SnackBar(content: Text()));
        }
        break;
      case MyConstants.engOffice:
        if (username.length > 0 && password.length > 0) {
          loadingBar();
          signInfo(sender);
        } else {
          showSB('يجب كتابة الحقول المطلوبة!');
          // Scaffold.of(_scaffoldContext).showSnackBar(
          //     SnackBar(content: Text()));
        }
        break;
      case MyConstants.user:
        if (username.length > 0 &&
            password.length > 0 &&
            full_name.length > 0 &&
            id_num.length > 0 &&
            phone.length > 0) {
          loadingBar();
          signInfo(sender);
        } else {
          showSB('يجب كتابة الحقول المطلوبة!');
          // Scaffold.of(_scaffoldContext).showSnackBar(
          //     SnackBar(content: Text()));
        }
        break;
      default:
    }
  }

  void navigatorGoTo(int sender) {
    switch (sender) {
      case MyConstants.employee:
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(
            builder: (context) => EmployeeMainPage(),
          ),
        );
        break;
      case MyConstants.engOffice:
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => EngOfficeMainPage(),
          ),
        );
        break;
      case MyConstants.user:
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => UserMainPage(),
          ),
        );
        break;
      default:
    }
  }

  void showSB(String msg) {
    Scaffold.of(_scaffoldContext)
      ..removeCurrentSnackBar()
      ..showSnackBar(SnackBar(content: Text(msg)));
  }

  void saveToSharedPreferences(String username) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(MyConstants.s_username, username);
  }
}
