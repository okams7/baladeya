import 'dart:async';

import 'package:baladeya/methods/api_services.dart';
import 'package:flutter/material.dart';

import 'package:baladeya/widgets/button_text.dart';
import 'package:baladeya/widgets/textfield_widget.dart';
import 'package:baladeya/methods/constants.dart';
import 'package:baladeya/methods/app_colors.dart';

class NewOrderPage extends StatefulWidget {
  NewOrderPage({this.sender});
  final int sender;

  @override
  _NewOrderPage createState() => _NewOrderPage(sender: sender);
}

class _NewOrderPage extends State<NewOrderPage> {
  String _fullName;
  String _app_date;
  String _street_num;
  String _location_num;
  String _land_num;
  String _office_num;
  String _tele_num;
  String _app_type = 'النوع';
  String _space_num;
  String _cost_num;
  BuildContext _scaffoldContext;

  var _heightController = TextEditingController();
  var _weightController = TextEditingController();
  var _formKey = GlobalKey<FormState>();
  final FocusNode _focusFullName = FocusNode();
  final FocusNode _focusAppDate = FocusNode();
  final FocusNode _focusStreet_num = FocusNode();
  final FocusNode _focusLocation_num = FocusNode();
  final FocusNode _focusLand_num = FocusNode();
  final FocusNode _focusOffice_num = FocusNode();
  final FocusNode _focusTele_num = FocusNode();
  final FocusNode _focusApp_type = FocusNode();
  final FocusNode _focusSpaceNum = FocusNode();
  final FocusNode _focusCostNum = FocusNode();
  String _age, _weight, _height;

  String dropdownValue = 'النوع';
  String notSelectedDropdownValue = 'النوع';

  var _weightMessage = "weight";

  var _heightMessage = "height";
  _NewOrderPage({this.sender});
  List<String> dropdownList = <String>['النوع', 'جديد', 'قديم'];

  final int sender;

  @override
  void initState() {
    print('sender: $sender');
    // double height = MediaQuery.of(context).size.height;
    super.initState();
  }

  // RaisedButton calculateButton() {
  //   return RaisedButton(
  //     onPressed: _calculator,
  //     color: Colors.pinkAccent,
  //     child: Text(
  //       'Calculate',
  //       style: TextStyle(fontSize: 16.9),
  //     ),
  //     textColor: Colors.white70,
  //   );
  // }

  Widget _saveButtonWidget() {
    return Container(
      height: 60,
      // width: 200,
      margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
      decoration: BoxDecoration(
          color: yellowApp,
          border: new Border.all(
              color: blueAppDark, width: 2.5, style: BorderStyle.solid),
          borderRadius: BorderRadius.all(Radius.circular(20.0))),

      child: ClipRRect(
        borderRadius: BorderRadius.all(Radius.circular(18.0)),
        child: Material(
          // shape: CircleBorder(),
          clipBehavior: Clip.antiAlias,
          color: Colors.transparent,
          child: InkWell(
            // splashColor: Colors.orange,
            highlightColor: Colors.orange,
            onTap: () {
              if (_formKey.currentState.validate()) {
                print('all valid');
                _formKey.currentState.save();
                if (_app_type != notSelectedDropdownValue) {
                  getRemoteData();
                }
                else{
                  showSB('تأكد من اختيار نوع الطلب');
                }

              }
              else{
                print('something missing');
                
              }
            },
            child: Center(
                child: Text("حفظ",
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 18, color: Colors.black54))),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    Widget body = Center(
      child: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                height: 16,
              ),
              fullNameFormField(context),
              appDateFormField(context),
              streetNumFormField(context),
              locationNumFormField(context),
              landNumFormField(context),
              officeNumFormField(context),
              teleNumFormField(context),
              spaceFormField(context),
              costFormField(context),
              _buildDropDown(),
              Padding(
                padding: EdgeInsets.only(top: 10.0),
                child: _saveButtonWidget(),
                // child: calculateButton(),
              ),
            ],
          ),
        ),
        // child: Column(
        //   crossAxisAlignment: CrossAxisAlignment.center,
        //   children: <Widget>[
        //     SizedBox(
        //       height: 8,
        //     ),
        //     SizedBox(
        //       height: 8,
        //     ),
        //     TextFieldWidget("الاسم", MyConstants.full_name, callBackText),
        //     SizedBox(
        //       height: 8,
        //     ),
        //     TextFieldWidget("التاريخ", MyConstants.app_date, callBackText),
        //     SizedBox(
        //       height: 8,
        //     ),
        //     TextFieldWidget("الشارع", MyConstants.street_num, callBackText),
        //     SizedBox(
        //       height: 8,
        //     ),
        //     TextFieldWidget("الموقع", MyConstants.location_num, callBackText),
        //     SizedBox(
        //       height: 8,
        //     ),
        //     TextFieldWidget("الحوض", MyConstants.land_num, callBackText),
        //     SizedBox(
        //       height: 8,
        //     ),
        //     TextFieldWidget("المكتب", MyConstants.office_num, callBackText),
        //     SizedBox(
        //       height: 8,
        //     ),
        //     TextFieldWidget("الهاتف", MyConstants.tele_num, callBackText),
        //     SizedBox(
        //       height: 8,
        //     ),
        //     _buildDropDown(),
        //     SizedBox(
        //       height: 8,
        //     ),
        //     TextFieldWidget("المساحة", MyConstants.engOffice, callBackText),
        //     SizedBox(
        //       height: 8,
        //     ),
        //     TextFieldWidget("التكلفة", MyConstants.engOffice, callBackText),
        //     SizedBox(
        //       height: 16,
        //     ),
        //     ButtonWidget("حفظ", MyConstants.employee, callBack),
        //     SizedBox(
        //       height: 48,
        //     ),
        //   ],
        // ),
      ),
    );
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'طلب جديد',
          style: TextStyle(fontSize: 16),
        ),
        iconTheme: IconThemeData(color: blueApp),
        textTheme: TextTheme(
          headline: TextStyle(color: blueApp),
          title: TextStyle(color: blueApp),
        ),
        backgroundColor: yellowApp,
      ),
      backgroundColor: blueApp,
      body: Builder(builder: (BuildContext context) {
        _scaffoldContext = context;
        return body;
      }),
    );
  }

  // TextFormField weightFormField() {
  //   return TextFormField(
  //     controller: _weightController,
  //     keyboardType: TextInputType.number,
  //     textInputAction: TextInputAction.done,
  //     focusNode: _weightFocus,
  //     onFieldSubmitted: (value){
  //       _weightFocus.unfocus();
  //       _calculator();
  //     },
  //     validator: (value) {
  //       if (value.length == 0 || double.parse(value) == 0.0) {
  //         return ('Weight is not valid. Weight > 0.0');
  //       }
  //     },
  //     onSaved: (value) {
  //       _weight = value;
  //     },
  //     decoration: InputDecoration(
  //         hintText: _weightMessage,
  //         labelText: _weightMessage,
  //         icon: Icon(Icons.menu),
  //         fillColor: Colors.white
  //     ),
  //   );
  // }

  // TextFormField heightFormField(BuildContext context) {
  //   return TextFormField(
  //     controller: _heightController,
  //     keyboardType: TextInputType.number,
  //     textInputAction: TextInputAction.next,
  //     focusNode: _heightFocus,
  //     onFieldSubmitted: (term) {
  //       _fieldFocusChange(context, _heightFocus, _weightFocus);
  //     },
  //     validator: (value) {
  //       if (value.length == 0 || double.parse(value) == 0.0) {
  //         return ('Height is not valid. Height > 0.0');
  //       }
  //     },
  //     onSaved: (value) {
  //       _height = value;
  //     },
  //     decoration: InputDecoration(
  //         hintText: _heightMessage,
  //         icon: Icon(Icons.assessment),
  //         fillColor: Colors.white,
  //     ),
  //   );
  // }

  Container fullNameFormField(BuildContext context) {
    Widget textFormField = TextFormField(
      // keyboardType: TextInputType.number,
      textInputAction: TextInputAction.next,
      focusNode: _focusFullName,
      onFieldSubmitted: (term) {
        _fieldFocusChange(context, _focusFullName, _focusAppDate);
      },
      validator: (value) {
        if (value.length == 0) {
          return ('حقل مطلوب');
        }
      },
      onSaved: (value) {
        _fullName = value;
      },
      decoration: InputDecoration(
        labelText: 'الاسم',
        fillColor: Colors.white,
      ),
    );
    return _buildYellowConatiner(textFormField);
  }

  Container appDateFormField(BuildContext context) {
    Widget textFormField = TextFormField(
      // keyboardType: TextInputType.number,
      textInputAction: TextInputAction.next,
      focusNode: _focusAppDate,
      onFieldSubmitted: (term) {
        _fieldFocusChange(context, _focusAppDate, _focusStreet_num);
      },
      validator: (value) {
        if (value.length == 0) {
          return ('حقل مطلوب');
        }
      },
      onSaved: (value) {
        _app_date = value;
      },
      decoration: InputDecoration(
        labelText: 'التاريخ',
        fillColor: Colors.white,
      ),
    );
    return _buildYellowConatiner(textFormField);
  }

  Container streetNumFormField(BuildContext context) {
    Widget textFormField = TextFormField(
      // keyboardType: TextInputType.number,
      textInputAction: TextInputAction.next,
      focusNode: _focusStreet_num,
      onFieldSubmitted: (term) {
        _fieldFocusChange(context, _focusStreet_num, _focusLocation_num);
      },
      validator: (value) {
        if (value.length == 0) {
          return ('حقل مطلوب');
        }
      },
      onSaved: (value) {
        _street_num = value;
      },
      decoration: InputDecoration(
        labelText: 'الشارع',
        fillColor: Colors.white,
      ),
    );
    return _buildYellowConatiner(textFormField);
  }

  Container locationNumFormField(BuildContext context) {
    Widget textFormField = TextFormField(
      // keyboardType: TextInputType.number,
      textInputAction: TextInputAction.next,
      focusNode: _focusLocation_num,
      onFieldSubmitted: (term) {
        _fieldFocusChange(context, _focusLocation_num, _focusLand_num);
      },
      validator: (value) {
        if (value.length == 0) {
          return ('حقل مطلوب');
        }
      },
      onSaved: (value) {
        _location_num = value;
      },
      decoration: InputDecoration(
        labelText: 'الموقع',
        fillColor: Colors.white,
      ),
    );
    return _buildYellowConatiner(textFormField);
  }

  Container landNumFormField(BuildContext context) {
    Widget textFormField = TextFormField(
      // keyboardType: TextInputType.number,
      textInputAction: TextInputAction.next,
      focusNode: _focusLand_num,
      onFieldSubmitted: (term) {
        _fieldFocusChange(context, _focusLand_num, _focusOffice_num);
      },
      validator: (value) {
        if (value.length == 0) {
          return ('حقل مطلوب');
        }
      },
      onSaved: (value) {
        _land_num = value;
      },
      decoration: InputDecoration(
        labelText: 'الحوض',
        fillColor: Colors.white,
      ),
    );
    return _buildYellowConatiner(textFormField);
  }

  Container officeNumFormField(BuildContext context) {
    Widget textFormField = TextFormField(
      // keyboardType: TextInputType.number,
      textInputAction: TextInputAction.next,
      focusNode: _focusOffice_num,
      onFieldSubmitted: (term) {
        _fieldFocusChange(context, _focusOffice_num, _focusTele_num);
      },
      validator: (value) {
        if (value.length == 0) {
          return ('حقل مطلوب');
        }
      },
      onSaved: (value) {
        _office_num = value;
      },
      decoration: InputDecoration(
        labelText: 'المكتب',
        fillColor: Colors.white,
      ),
    );
    return _buildYellowConatiner(textFormField);
  }

  Container teleNumFormField(BuildContext context) {
    Widget textFormField = TextFormField(
      // keyboardType: TextInputType.number,
      textInputAction: TextInputAction.next,
      focusNode: _focusTele_num,
      onFieldSubmitted: (term) {
        _fieldFocusChange(context, _focusTele_num, _focusSpaceNum);
        // _focusTele_num.unfocus();
      },
      validator: (value) {
        if (value.length == 0) {
          return ('حقل مطلوب');
        }
      },
      onSaved: (value) {
        _tele_num = value;
      },
      decoration: InputDecoration(
        labelText: 'الهاتف',
        fillColor: Colors.white,
      ),
    );
    return _buildYellowConatiner(textFormField);
  }

  Container spaceFormField(BuildContext context) {
    Widget textFormField = TextFormField(
      // keyboardType: TextInputType.number,
      textInputAction: TextInputAction.next,
      focusNode: _focusSpaceNum,
      onFieldSubmitted: (term) {
        _fieldFocusChange(context, _focusSpaceNum, _focusCostNum);
      },
      validator: (value) {
        if (value.length == 0) {
          return ('حقل مطلوب');
        }
      },
      onSaved: (value) {
        _space_num = value;
      },
      decoration: InputDecoration(
        labelText: 'المساحة',
        fillColor: Colors.white,
      ),
    );
    return _buildYellowConatiner(textFormField);
  }

  Container costFormField(BuildContext context) {
    Widget textFormField = TextFormField(
      // keyboardType: TextInputType.number,
      textInputAction: TextInputAction.next,
      focusNode: _focusCostNum,
      onFieldSubmitted: (term) {
        // _fieldFocusChange(context, _focusCostNum, _focusFullName);
        _focusCostNum.unfocus();
      },
      validator: (value) {
        if (value.length == 0) {
          return ('حقل مطلوب');
        }
      },
      onSaved: (value) {
        _cost_num = value;
      },
      decoration: InputDecoration(
        labelText: 'التكلفة',
        fillColor: Colors.white,
      ),
    );
    return _buildYellowConatiner(textFormField);
  }

  Widget _buildYellowConatiner(TextFormField textf) {
    return Container(
      height: 60,
      // width: 200,
      margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
      decoration: BoxDecoration(
          color: yellowApp,
          border: new Border.all(
              color: Colors.white, width: 0, style: BorderStyle.solid),
          borderRadius: BorderRadius.all(Radius.circular(20.0))),
      child: Container(
        margin: EdgeInsets.only(left: 16, right: 16),
        child: Directionality(
          textDirection: TextDirection.rtl,
          child: textf,
        ),
      ),
    );
  }

  Widget _buildDropDown() {
    return Row(
      children: <Widget>[
        Expanded(
          child: Container(
            height: 60,
            // width: 200,
            margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
            decoration: BoxDecoration(
                color: yellowApp,
                border: new Border.all(
                    color: Colors.white, width: 0, style: BorderStyle.solid),
                borderRadius: BorderRadius.all(Radius.circular(20.0))),
            child: Container(
              margin: EdgeInsets.only(left: 16, right: 16),
              child: DropdownButtonHideUnderline(
                child: new DropdownButton<String>(
                  value: dropdownValue,
                  isDense: true,
                  onChanged: (String newValue) {
                    setState(() {
                      // newContact.favoriteColor = newValue;
                      dropdownValue = newValue;
                      _app_type = newValue;
                      // state.didChange(newValue);
                    });
                  },
                  items: dropdownList.map((String value) {
                    return new DropdownMenuItem<String>(
                      value: value,
                      child: new Text(value),
                    );
                  }).toList(),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  // void callBackText(int sender, String text) {
  //           print('test: $text');
  //           // _fullName,_app_date,_street_num,_location_num,_land_num,_office_num,_tele_num,_app_type
  //           switch (sender) {
  //             case MyConstants.full_name:
  //               _fullName = text;
  //               break;
  //             case MyConstants.app_date:
  //               _app_date = text;
  //               break;
  //               case MyConstants.street_num:
  //               _street_num = text;
  //               break;
  //               case MyConstants.location_num:
  //               _location_num = text;
  //               break;
  //               case MyConstants.land_num:
  //               _land_num = text;
  //               break;
  //               case MyConstants.office_num:
  //               _office_num = text;
  //               break;
  //               case MyConstants.tele_num:
  //               _tele_num = text;
  //               break;
  //             default:
  //           }
  //         }

  void callBack(int sender) {
    if (_fullName.length > 0 &&
        _app_date.length > 0 &&
        _app_type != notSelectedDropdownValue) {
      getRemoteData();
    } else {
      showSB('يجب كتابة الحقول المطلوبة!');
      // Scaffold.of(_scaffoldContext).showSnackBar(
      //     SnackBar(content: Text()));
    }
  }

  void getRemoteData() async {
    loadingBar(true);
    final model = await insertApp(_fullName, _app_date, _street_num,
        _location_num, _land_num, _office_num, _tele_num, _app_type);
    loadingBar(false);
    //
    if (model != null) {
      print('ZLoginModel:${model.res.toString()}');
      if (model.res == 1) {
        print('Successful');
        Navigator.pop(context);
        // showSB('عدد الطلبات : ${model.data.length}');
        // Scaffold.of(_scaffoldContext).showSnackBar(
        //     SnackBar(content: Text('عدد الطلبات : ${model.data.length}')));
      } else {
        print('Unsuccessful');
        showSB('خطأ');
        // Scaffold.of(_scaffoldContext)
        //     .showSnackBar(SnackBar(content: Text('خطأ')));
      }
    } else {
      print('ZLoginModel:null');
      showSB('حدث خطأ ما.. ');
      // Scaffold.of(_scaffoldContext)
      //     .showSnackBar(SnackBar(content: Text('حدث خطأ ما.. ')));
    }
  }

  void loadingBar(bool show) async {
    await Future.delayed(Duration(milliseconds: 50));
    if (show) {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return Center(
            child: Container(
              height: 100,
              width: 100,
              margin: EdgeInsets.only(left: 16, right: 16),
              decoration: BoxDecoration(
                  color: Colors.black.withOpacity(0.5),
                  // border: new Border.all(
                  //     color: Colors.black, width: 2.5, style: BorderStyle.solid),
                  borderRadius: BorderRadius.all(Radius.circular(20.0))),
              child: Center(
                child: CircularProgressIndicator(),
              ),
            ),
          );
        },
      );
    } else {
      Navigator.pop(context);
    }
  }

  void showSB(String msg) {
    Scaffold.of(_scaffoldContext)
      ..removeCurrentSnackBar()
      ..showSnackBar(SnackBar(content: Text(msg)));
  }

  _fieldFocusChange(
      BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }
}
