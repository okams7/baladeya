import 'package:flutter/material.dart';

import 'package:baladeya/widgets/button_text.dart';
import 'package:baladeya/widgets/textfield_icon_widget.dart';
import 'package:baladeya/pages/signup_page.dart';
import 'package:baladeya/methods/constants.dart';
import 'package:baladeya/pages/employee_main.dart';
import 'package:baladeya/pages/eng_office_main.dart';
import 'package:baladeya/pages/user_main.dart';
import 'package:baladeya/methods/api_services.dart';
import 'package:baladeya/methods/app_colors.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginPage extends StatefulWidget {
  LoginPage({this.sender});
  final int sender;

  @override
  _LoginPageState createState() => _LoginPageState(sender: sender);
}

class _LoginPageState extends State<LoginPage> {
  bool isLoading = false;
  BuildContext _scaffoldContext;
  _LoginPageState({this.sender});
  final int sender;
  String userTitle;
  String passTitle;
  String loginTitle;
  String signupTitle;
  int loginSender;
  int signupSender;
  String username = "";
  String password = "";
  String bkgImg = 'assets/employment_login.jpg';

  @override
  void initState() {
    print('sender: $sender');
    switch (sender) {
      case MyConstants.employee:
        userTitle = "الرقم الوظيفي";
        passTitle = "كلمة السر";
        loginTitle = "تسجيل الدخول";
        signupTitle = "موظف جديد";
        bkgImg = 'assets/employment_login.jpg';
        loginSender = MyConstants.employeeLogin;
        signupSender = MyConstants.employeeSignup;
        break;
      case MyConstants.engOffice:
        userTitle = "البريد الالكتروني";
        passTitle = "كلمة السر";
        loginTitle = "تسجيل الدخول";
        signupTitle = "شركة جديدة";
        bkgImg = 'assets/company_login.jpg';
        loginSender = MyConstants.engOfficeLogin;
        signupSender = MyConstants.engOfficeSignup;
        break;
      case MyConstants.user:
        userTitle = "اسم المستخدم";
        passTitle = "كلمة السر";
        loginTitle = "تسجيل الدخول";
        signupTitle = "مستخدم جديد";
        bkgImg = 'assets/user_login.jpg';
        loginSender = MyConstants.userLogin;
        signupSender = MyConstants.userSignup;
        break;
      default:
        userTitle = "خطأ";
        passTitle = "خطأ";
        loginTitle = "خطأ";
        signupTitle = "خطأ";
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Widget body = Stack(
      children: <Widget>[
        Container(
          // color: Colors.blueGrey,
          // margin: EdgeInsets.only(top: 80),
          decoration: new BoxDecoration(
            image: new DecorationImage(
              image: new AssetImage(bkgImg),
              fit: BoxFit.fill,
            ),
          ),
        ),

        // Align(
        //   alignment: FractionalOffset.bottomCenter,
        //   child: Container(
        //     height: 200,
        //     decoration: new BoxDecoration(
        //       image: new DecorationImage(
        //         image: new AssetImage('assets/graduation.jpg'),
        //         fit: BoxFit.cover,
        //       ),
        //     ),
        //   ),
        // ),
        Center(
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                // SizedBox(
                //   height: 60,
                // ),
                // Center(
                //   child: LogoWidget(),
                // ),
                SizedBox(
                  height: 60,
                ),
                TextFieldIconWidget(userTitle, MyConstants.username,
                    Icon(Icons.person), false, callBackText),
                SizedBox(
                  height: 8,
                ),
                TextFieldIconWidget(passTitle, MyConstants.password,
                    Icon(Icons.dvr), true, callBackText),
                SizedBox(
                  height: 16,
                ),
                _buttonsRow(),
              ],
            ),
          ),
        ),

        // loadingBar(),
        Positioned(
          top: 0,
          left: 0,
          right: 0,
          child: AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0,
            iconTheme: IconThemeData(color: yellowApp),
          ),
        ),
      ],
    );
    return Scaffold(
      // appBar:
      body: new Builder(builder: (BuildContext context) {
        _scaffoldContext = context;
        return body;
      }),
    );
  }

  void loadingBar() {
    // if (isLoading) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return Center(
          child: Container(
            height: 100,
            width: 100,
            margin: EdgeInsets.only(left: 16, right: 16),
            decoration: BoxDecoration(
                color: Colors.black.withOpacity(0.5),
                // border: new Border.all(
                //     color: Colors.black, width: 2.5, style: BorderStyle.solid),
                borderRadius: BorderRadius.all(Radius.circular(20.0))),
            child: Center(
              child: CircularProgressIndicator(),
            ),
          ),
        );
      },
    );
    // }
    // else {
    //   return SizedBox();
    // }

    // if (isLoading) {
    //   return Center(
    //     child: Container(
    //       height: 100,
    //       width: 100,
    //       margin: EdgeInsets.only(left: 16, right: 16),
    //       decoration: BoxDecoration(
    //           color: Colors.black.withOpacity(0.5),
    //           // border: new Border.all(
    //           //     color: Colors.black, width: 2.5, style: BorderStyle.solid),
    //           borderRadius: BorderRadius.all(Radius.circular(20.0))),
    //       child: Center(
    //         child: CircularProgressIndicator(),
    //       ),
    //     ),
    //   );
    // } else
    //   return SizedBox();
  }

  Widget _buttonsRow() {
    return Row(
      children: <Widget>[
        Expanded(
          child: ButtonWidget(loginTitle, loginSender, callBack),
        ),
        Expanded(
          child: ButtonWidget(signupTitle, signupSender, callBack),
        ),
      ],
    );
  }

  void loginInfo(int sender) async {
    final loginModel = await getLogin(username, password);
    Navigator.pop(context);
    if (loginModel != null) {
      print('ZLoginModel:${loginModel.id}');
      if (loginModel.res == 1) {
        print('Successful');
        saveToSharedPreferences(username);
                switch (sender) {
                  case MyConstants.employeeLogin:
                    Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                        builder: (context) => EmployeeMainPage(),
                      ),
                    );
                    break;
                  case MyConstants.engOfficeLogin:
                    Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                        builder: (context) => EngOfficeMainPage(),
                      ),
                    );
                    break;
                  case MyConstants.userLogin:
                    Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                        builder: (context) => UserMainPage(),
                      ),
                    );
                    break;
                  default:
                }
              } else {
                print('Unsuccessful');
                showSB("اسم المستخدم او كلمة المرور غير صحيحة!");
                // Scaffold.of(_scaffoldContext)..removeCurrentSnackBar()
                // .showSnackBar(
                //     SnackBar(content: Text('')));
              }
            } else {
              print('ZLoginModel:null');
              showSB('حدث خطأ ما.. تأكد من اتصالك بالانترنت');
              // Scaffold.of(_scaffoldContext).showSnackBar(
              //     SnackBar(content: Text()));
            }
          }
        
          void showSB(String msg) {
            Scaffold.of(_scaffoldContext)
              ..removeCurrentSnackBar()
              ..showSnackBar(SnackBar(content: Text(msg)));
          }
        
          void callBackText(int sender, String text) {
            print('test: $text');
            switch (sender) {
              case MyConstants.username:
                username = text;
                break;
              case MyConstants.password:
                password = text;
                break;
              default:
            }
          }
        
          void callBack(int sender) {
            print('callback: $sender');
            switch (sender) {
              case MyConstants.employeeLogin:
                if (username.length > 0 && password.length > 0) {
                  loadingBar();
                  // setState(() {
                  //   // isLoading = true;
                  //   loadingBar();
                  // });
                  loginInfo(sender);
                } else {
                  showSB('يجب كتابة الحقول المطلوبة!');
                  // Scaffold.of(_scaffoldContext).showSnackBar(
                  //     SnackBar(content: Text()));
                }
                break;
              case MyConstants.employeeSignup:
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => SignUpPage(sender: MyConstants.employee),
                  ),
                );
                break;
              case MyConstants.engOfficeSignup:
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => SignUpPage(sender: MyConstants.engOffice),
                  ),
                );
                break;
              case MyConstants.engOfficeLogin:
                if (username.length > 0 && password.length > 0) {
                  loadingBar();
                  loginInfo(sender);
                } else {
                  showSB('يجب كتابة الحقول المطلوبة!');
                  // Scaffold.of(_scaffoldContext).showSnackBar(
                  //     SnackBar(content: Text()));
                }
                break;
              case MyConstants.userSignup:
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => SignUpPage(sender: MyConstants.user),
                  ),
                );
                break;
              case MyConstants.userLogin:
                if (username.length > 0 && password.length > 0) {
                  loadingBar();
                  loginInfo(sender);
                } else {
                  showSB('يجب كتابة الحقول المطلوبة!');
                  // Scaffold.of(_scaffoldContext).showSnackBar(
                  //     SnackBar(content: Text()));
                }
                break;
              default:
            }
          }
        
          void saveToSharedPreferences(String username) async {
            final prefs = await SharedPreferences.getInstance();
            prefs.setString(MyConstants.s_username, username);
          }
}
