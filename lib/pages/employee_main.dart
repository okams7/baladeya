import 'package:flutter/material.dart';
import 'package:baladeya/methods/constants.dart';
import 'package:baladeya/widgets/button_text.dart';
import 'package:baladeya/pages/control_orders_page.dart';
import 'package:baladeya/pages/control_prices_page.dart';
import 'package:baladeya/methods/app_colors.dart';


class EmployeeMainPage extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return _EmployeeMainPage();
  }
}

class _EmployeeMainPage extends State<EmployeeMainPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'الصفحة الرئيسية',
          style: TextStyle(fontSize: 16),
        ),
        iconTheme: IconThemeData(color: blueApp),
        textTheme: TextTheme(
          headline: TextStyle(color: blueApp),
          title: TextStyle(color: blueApp),
        ),
        backgroundColor: yellowApp,
      ),
      backgroundColor: blueApp,
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              ButtonWidget("التحكم بالطلبات", MyConstants.controlOrders, callBack),
              SizedBox(
                height: 16,
              ),
              ButtonWidget("التحكم بالاسعار", MyConstants.controlPrices, callBack),
              SizedBox(
                height: 16,
              ),
              ButtonWidget("الاجازات", MyConstants.holidaies, callBack),
              SizedBox(
                height: 16,
              ),
            ],

            //
          ),
        ),
      ),
    );
  }

  void callBack(int sender) {
    switch (sender) {
      case MyConstants.controlOrders:
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => ControlOrdersPage(),
          ),
        );
        break;
        case MyConstants.controlPrices:
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => ControlPricesPage(),
          ),
        );
        break;
      default:
    }
  }
}
